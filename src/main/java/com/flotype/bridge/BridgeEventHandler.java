package com.flotype.bridge;

public class BridgeEventHandler {

	public void onReady() {
	}

	public void onRemoteError(String msg) {
	}

	public void onReconnect() {
	}
}
