package com.flotype.bridge.example.adder;

import com.flotype.bridge.BridgeRemoteObject;

public interface AdderHandler extends BridgeRemoteObject {

	public void greeting();

}
