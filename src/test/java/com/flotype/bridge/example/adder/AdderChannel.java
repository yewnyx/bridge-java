package com.flotype.bridge.example.adder;

import com.flotype.bridge.BridgeObject;

public class AdderChannel implements BridgeObject {

	public void greeting() {
		System.out.println("Hello there!");
	}

}
