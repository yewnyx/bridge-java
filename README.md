#Flotype Bridge for Java
Flotype Bridge enables cross-language RPC for interserver and client-server communication.

##Installation
Quick install: Using [Apache Maven](http://maven.apache.org/), include the Flotype Bridge artifact in your `pom.xml` file

    <groupId>org.mockito</groupId>
    <artifactId>mockito-all</artifactId>
    <version>1.9.0</version>
    <scope>test</scope>

JAR install without Maven: A JAR file containing Flotype Bridge and all
depencies is available from
[Flotype](http://cloud.flotype.com/bridge.jar)

Source install: 

Clone this repository using `git clone git@bitbucket.org:flotype/bridge-java.git` and install using `mvn install` for Maven users. 

###Dependencies
This library has no external dependencies.

##Documentation and Support
* API Reference: http://www.flotype.com/resources/api
* Getting Started: http://www.flotype.com/resources/gettingstarted
* About Flotype and Flotype Bridge: http://www.flotype.com/

The `examples` directory of this library contains sample applications for Flotype Bridge.

Support is available in #flotype on Freenode IRC or the Flotype Bridge Google Group.


##License
Flotype Bridge is made available under the MIT/X11 license. See LICENSE file for details.

